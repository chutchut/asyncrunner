// Copyright (c) 2020 Calum Ewart Hutton
// Distributed under the GNU General Public License v3.0+, see the accompanying
// file LICENSE or https://opensource.org/licenses/GPL-3.0.

package lib.chutchut.asyncrunner;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class TaskRunner {

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private Handler handler = new Handler(Looper.getMainLooper());

    public TaskRunner() {}

    protected void onPreExecute() {}

    protected abstract Object doTask();

    protected void onPostExecute(boolean status, Object data) {}

    public void execute() {
        executor.execute(() -> {
            handler.post(this::onPreExecute);

            boolean status = true;
            Object data = null;
            try {
                data = doTask();
            } catch (Exception e) {
                status = false;
            }

            boolean finalStatus = status;
            Object finalData = data;
            handler.post(() -> {
                onPostExecute(finalStatus, finalData);
            });

        });
    }

}
